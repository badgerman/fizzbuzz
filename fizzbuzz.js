const fizzBuzz = (args)=>{
    //config for number of itteration
    const {iterations = 100} = args

    //config critera and make it reuseable
    const {criteria = []} = args

    //loop through iteration
    for (let i = 1; i <= iterations; i++) {

        //create a empty string fior output    
        let output = i

        //apply critera
        criteria.map((rule)=>{
            if((i % rule.multiple) === 0){
                //reset output if it = i
                if(output == i){
                    output = ""
                }
                output += rule.output
            }
        })

        //print output
        console.log(output); 
    }
}

fizzBuzz({
    iterations: 100,
    criteria:[
        {
            multiple: 3,
            output: "Fizz" 
        },
        {
            multiple: 5,
            output: "Buzz" 
        }
    ]
})